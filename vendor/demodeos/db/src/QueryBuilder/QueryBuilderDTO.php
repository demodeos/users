<?php
declare(strict_types=1);

namespace Demodeos\DB\QueryBuilder;

class QueryBuilderDTO
{
    public string $sql;
    public array $params;

}