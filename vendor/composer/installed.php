<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'demodeos/users',
        'dev' => true,
    ),
    'versions' => array(
        'demodeos/db' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../demodeos/db',
            'aliases' => array(
                0 => '9999999-dev',
            ),
            'reference' => '97917ad4555be6eb2e209b9d5532534fae57d312',
            'dev_requirement' => false,
        ),
        'demodeos/users' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
    ),
);
