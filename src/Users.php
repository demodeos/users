<?php
declare(strict_types=1);

namespace Demodeos\Users;

use Demodeos\Users\DTO\RegistrationDTO;
use Demodeos\Users\DTO\UserDTO;
use Demodeos\Users\DTO\UserLayerDTO;

class Users
{
    public ?DB $_sql = null;


    public function __construct($sql = null)
    {

        if(is_null($sql))
        {
            $config = require_once __DIR__.'/config.php';
            $this->_sql ??= new DB($config);
        }
        else
        {
            $this->_sql ??= $sql;
        }


    }


    public function login(UserDTO $user = null)
    {
        $result = (new Authorization($this->_sql, $user));





    }

    public function logout(UserDTO $user)
    {



    }

    public function registration(RegistrationDTO $user = null): UserLayerDTO
    {
        $result = (new Registration($this->_sql, $user))->init();

        return $result;

    }








}