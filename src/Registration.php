<?php
declare(strict_types=1);

namespace Demodeos\Users;

use Demodeos\DB\Connection;
use Demodeos\DB\QueryBuilder\QueryBuilder;
use Demodeos\Users\DTO\RegistrationDTO;
use Demodeos\Users\DTO\UserDTO;
use Demodeos\Users\DTO\UserLayerDTO;
use Demodeos\Users\DTO\UserModel;
use ReflectionClass;
use ReflectionProperty;

class Registration
{
    private DB $_sql;
    private ?RegistrationDTO $_registration_dto;

    public function __construct($sql, ?RegistrationDTO $user = null)
    {
        $this->_sql = $sql;
        $this->_registration_dto = $user;



    }



    public function init()
    {
        if(is_null($this->_registration_dto))
        {
            $properties = (new ReflectionClass(RegistrationDTO::class))->getProperties(ReflectionProperty::IS_PUBLIC);

            $this->_registration_dto = new RegistrationDTO();

            foreach ($properties as $value)
            {
               if(isset($_POST[$value->name]))
               {
                   $this->_registration_dto->{$value->name} = $_POST[$value->name];
               }
            }
        }


       $validate = $this->_registration_dto->validate();

        if(!$validate)
        {
            $return = new UserLayerDTO();
            $return->error = true;
            $return->body = $this->_registration_dto->getMessage();

            return $return;
        }


        $model = new UserModel();
        $model->email = $this->_registration_dto->email;
        $model->password = password_hash($this->_registration_dto->password, PASSWORD_DEFAULT);
        $model->username = $model->email;
        $model->status = 1;
        $model->app_token = token(64);
        $model->confirm_token = token(32);
        $model->role = 2;
        unset($model->guid, $model->id, $model->created_at, $model->updated_at);


      return  $this->registration($model);

    }

    public function registration(UserModel $user): RegistrationDTO|UserLayerDTO
    {
        $SQL = QueryBuilder::insert($user)->table('users')->create();

        $return = new UserLayerDTO();

        $result = $this->_sql->query($SQL->sql, $SQL->params);

        if($result->error)
        {
            $return->error = true;
            $return->body = $result->message;
        }
        else
        {
            $id = $result->lastID();
            $SQL = "SELECT * FROM users WHERE id = ".$id;
            $result = $this->_sql->query($SQL)->fetchAll(UserDTO::class);
            $return->body = $result;
        }

        return $return;

    }



}