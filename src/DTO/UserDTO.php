<?php
declare(strict_types=1);

namespace Demodeos\Users\DTO;


class UserDTO
{
    public $id;
    public $guid;
    public $app_token;
    public $username;
    public $password;
    public $email;
    public $status;
    public $role;
    public $confirm_token;
    public $created_at;
    public $updated_at;


    public static function load(array $data = null): UserDTO
    {
        $user = new UserDTO();



        return $user;
    }

}