<?php
declare(strict_types=1);

namespace Demodeos\Users\DTO;

class RegistrationDTO
{

    public string $password;
    public string $email;

    private bool $error = false;
    private string $message = '';

    public function validate()
    {


        if(isset($this->email))
        {
            if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
            {
                $this->error = true;
                $this->message .= 'Некорретное поле email.';
            }
        }
        else
        {
            $this->error = true;
            $this->message .= 'Поле email - обязательно для заполнения.';
        }

        if(!isset($this->password))
        {
            $this->error = true;
            $this->message .= 'Поле пароль - обязательно для заполнения.';
        }
        else
            if(strlen($this->password)<6)
            {
                $this->error = true;
                $this->message .='Поле пароль должно содержать не менее 6 символов';
            }
        return !$this->error;

    }

    public function getMessage(): string
    {
        return $this->message;
    }
    public function getError(): bool
    {
        return $this->error;
    }

}